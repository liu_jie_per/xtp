import { Base64 } from "js-base64";

const JsBase64 = {
  encode(value = "") {
    console.log(value, Base64.encode(value), "JsBase64-encode");

    if (!value) return "";
    return Base64.encode(value);
  },
  decode(value = "") {
    console.log(value, Base64.decode(value), "解码");

    if (!value) return "";
    return Base64.decode(value);
  },
};
export default JsBase64;
