import service from "@/utils/request";

const ContentTypes = [
  "application/json;charset=UTF-8",
  "application/x-www-form-urlencoded;charset=UTF-8",
];

/**
 * GET请求获取数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Get(api, data = {}, type = 0) {
  return service({
    url: api,
    params: data,
    method: "get",
    headers: { "Content-Type": ContentTypes[type] },
  });
}

/**
 * POST请求提交数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Post(api, data = {}, type = 0) {
  return service({
    url: api,
    data: data,
    method: "post",
    headers: { "Content-Type": ContentTypes[type] },
  });
}

/**
 * PUT请求更新数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Put(api, data = {}, type = 0) {
  return service({
    url: api,
    data: data,
    method: "put",
    headers: { "Content-Type": ContentTypes[type] },
  });
}

/**
 * DELETE请求删除数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Delete(api, data = {}, type = 0) {
  return service({
    url: api,
    data: data,
    method: "delete",
    headers: { "Content-Type": ContentTypes[type] },
  });
}

/**
 * PATCH请求提交数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Patch(api, data = {}, type = 0) {
  return service({
    url: api,
    data: data,
    method: "patch",
    headers: { "Content-Type": ContentTypes[type] },
  });
}

/**
 * Export导出数据
 * @param {*} api
 * @param {*} data
 * @param {*} type
 * @returns
 */
export function Export(api, data = {}, type = 0) {
  return service({
    url: api,
    data: data,
    method: "get",
    responseType: "blob",
  });
}
