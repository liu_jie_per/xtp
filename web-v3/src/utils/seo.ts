import { updateOrCreateMetaTag } from "@/utils/metaTagger";
import tools from "@/utils/tools";

export const seoFun = (seoReponse) => {
  document.title = seoReponse.title || "";
  updateOrCreateMetaTag("description", seoReponse.desc || "");
  updateOrCreateMetaTag("keywords", seoReponse.keyword || "");
  tools.translate();
};

export default function (path) {
  return new Promise(async (resolve) => {
    let seoReponse = { title: "", desc: "", keyword: "" };
    switch (path) {
      case "/888":
        break;
      default:
        break;
    }
    seoFun(seoReponse);
    resolve();
  });
}
