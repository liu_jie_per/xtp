export const TokenKey = "token";

export const setToken = (token) => {
  return sessionStorage.setItem("token", token);
};

export const getToken = () => {
  return sessionStorage.getItem("token");
};

export const removeToken = () => {
  return sessionStorage.removeItem("token");
};

export function getLocalStorage(key) {
  return localStorage.getItem(key);
}

export function setLocalStorage(key, storage) {
  localStorage.setItem(key, storage);
}

export function removeLocalStorage(key) {
  localStorage.removeItem(key);
}
