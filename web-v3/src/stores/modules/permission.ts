import { defineStore } from "pinia";
import { ref, computed } from "vue";

// 用户菜单获取
const userMenuList = [
  {
    id: 1,
    path: "/home",
    name: "Home",
    title: "主页",
    children: [
      {
        id: 1,
        path: "/detail",
        name: "Home",
        title: "主页",
        children: [],
      },
    ],
  },
  {
    id: 1,
    path: "/home",
    name: "Home",
    title: "主页",
    children: [],
  },
];

export const useRouterPermissionStore = defineStore("routerPermission", () => {
  let menuListRef = [];

  const menuList = computed(() => menuListRef);

  const getMenuList = async function () {
    console.log("执行1");
    const promise = new Promise((resolve) => {
      setTimeout(() => {
        console.log("执行2");
        menuListRef = userMenuList;
        console.log(menuListRef, menuList);
        resolve();
      }, 1000);
    });
    await promise;
    console.log("执行3");
    return true;
  };

  return { menuList, getMenuList, menuListRef };
});
