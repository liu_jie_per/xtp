import { createRouter, createWebHistory } from "vue-router";
import { basicRoutes, AllRoute } from "./routes";
import seo from "@/utils/seo";
import tools from "@/utils/tools";
import { getLocalStorage, setLocalStorage } from "@/utils/auth";

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [...basicRoutes, ...AllRoute],
  // strict: true,
  scrollBehavior: (to, from, savedPosition) => {
    console.log("scrollBehavior-行为", savedPosition);
    setLocalStorage(to.name + "scroll", savedPosition ? savedPosition.top : 0);
    if (savedPosition) return savedPosition;
    return { left: 0, top: 0 };
  },
});

router.beforeEach(async (to, from, next) => {
  await seo(to.path);
  next();
});
router.afterEach(async (to, from, next) => {
  tools.translate();
});

export default router;
