// 404 页面
export const PAGE_NOT_FOUND_ROUTE = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login.vue"),
  },
  {
    path: "/:pathMatch(.*)",
    redirect: "/404",
  },
];
