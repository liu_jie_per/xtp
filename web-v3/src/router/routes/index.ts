import { PAGE_NOT_FOUND_ROUTE } from "./basic";

// 基础菜单
export const basicRoutes = [...PAGE_NOT_FOUND_ROUTE];

export const AllRoute = [
  {
    name: "layout",
    path: "/",
    component: () => import("@/layout/default.vue"),
    meta: {
      roleCode: "1-*",
      role: [""],
    },
    children: [
      {
        path: "/404",
        name: "404",
        component: () => import("@/layout/404.vue"),
      },
      {
        path: "/home",
        name: "Home",
        component: () => import("@/views/home.vue"),
        meta: {
          roleCode: "1-1",
          role: [""],
        },
        children: [],
      },
      {
        path: "/detail",
        name: "HomeDetail",
        component: () => import("@/views/login.vue"),
        meta: {
          keepAlive: true,
        },
        children: [],
      },
    ],
  },
];
