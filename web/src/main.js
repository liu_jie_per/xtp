import Vue from 'vue'
import Router from 'vue-router'
import routers from './router'
import store from "./store"
// @ts-ignore
import App from './App.vue'
import ElementUI from 'element-ui'
import Components from "./components";
import "./assets/styles/scss/themes/element-variables.scss"
import "font-awesome/css/font-awesome.min.css"
import 'babel-polyfill'
import "./utils/common";
import VueI18n from 'vue-i18n';
import Messages from "./utils/i18n/index.js";
import Operation from "./mixins/operation";
import Enum from "./utils/enum";

Vue.use(Router);
Vue.use(ElementUI);
Vue.use(Components);
Vue.use(VueI18n); //多语言

Vue.config.productionTip = false;

Vue.mixin(Operation);/* 挂载全局混用判断是否有操作权限事件*/

// ts定义
// declare global {
//   interface Window {
//     SYSTEM_NAME: string
//     _Vue: any
//   }
// }

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => err);
};
const router = new Router({
  routes: routers
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const getStorageItem = sessionStorage.getItem(window.TOKEN_KEY) || localStorage.getItem(window.TOKEN_KEY) || null;
    if (!getStorageItem) {
      next({
        path: "/login"
        // query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
    next();
  } else {
    next();
  }
});

const locale = window.localStorage.getItem("locale");
const i18n = new VueI18n({
  locale: locale || "zh-CN",
  messages: Messages
});
if (!locale) {
  window.localStorage.setItem("locale", i18n.locale);
}

window.document.title = window.THEME_CONFIG.NAV_BAR_NAME;

/* eslint-disable no-new */
window._Vue = new Vue({
  el: '#app',
  i18n,
  router,
  store,
  data: {
    Bus: new Vue()
  },
  render: (h) => h(App)
})
