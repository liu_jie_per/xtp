import Area from "./area/area.vue";
import Custom from "./custom/custom.vue";
import Farmer from "./custom/farmer.vue";
import Dict from "./dict/dict.vue";
import MeasureUnit from "./measure-unit/measure-unit.vue";
import Supplier from "./supplier/supplier.vue";
import Warehouse from "./warehouse/warehouse.vue";
import DictRoleBind from "./dict/dict-role-bind.vue";
import FormConfig from "./form-config/form-config.vue";
import CodingRule from "./coding-rule/coding-rule.vue";

export const components = [
  {title: "区域管理", name: "区域管理", component: Area, path: "/ibasic/area"},
  {title: "药农管理", name: "药农管理", component: Farmer, path: "/ibasic/farmer"},
  {title: "客户管理", name: "客户管理", component: Custom, path: "/ibasic/custom"},
  {title: "字典管理", name: "字典管理", component: Dict, path: "/ibasic/dict"},
  {title: "角色绑定", name: "角色绑定", component: DictRoleBind, path: "/ibasic/dict-role-bind"},
  {title: "计量单位", name: "计量单位", component: MeasureUnit, path: "/ibasic/measure-unit"},
  {title: "供应商", name: "供应商", component: Supplier, path: "/ibasic/supplier"},
  {title: "仓库资料", name: "仓库资料", component: Warehouse, path: "/ibasic/warehouse"},
  {title: "表单配置", name: "表单配置", component: FormConfig, path: "/ibasic/form-config"},
  {title: "编码规则", name: "编码规则", component: CodingRule, path: "/ibasic/coding-rule"}
];
