import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Dict = function() {
  return {
    id: 0,
    type: null,
    key: null,
    value: null,
    keyName: null,
    grouping: null,
    remark: null,
    editFlag: null,
    displayFlag: null,
    sort: null
  };
},
DictAPI = {
  dict: new Dict(),
  get: () => DictAPI.dict,
  set: (value) => {
    DictAPI.dict = Object.assign(DictAPI.dict, value);
  },
  init: () => new Dict(),
  getDict(params) {
    return new Promise((resolve) => {
      IBASIC.dict.getDict(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listDict(params) {
    return new Promise((resolve) => {
      IBASIC.dict.listDict(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertDict(params) {
    return new Promise((resolve) => {
      IBASIC.dict.insertDict(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateDict(params) {
    return new Promise((resolve) => {
      IBASIC.dict.updateDict(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteDict(params) {
    return new Promise((resolve) => {
      IBASIC.dict.deleteDict(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { DictAPI };
