import { IBASIC } from "../../api";
import { Message } from "element-ui";

const CodingRule = function() {
  return {
    id: 0,
    key: null,
    ruleCode: null,
    ruleName: null,
    type: null,
    length: null,
    initValue: null,
    format: null,
    sort: null
  };
},
CodingRuleAPI = {
  codingRule: new CodingRule(),
  get: () => CodingRuleAPI.codingRule,
  set: (value) => {
    CodingRuleAPI.codingRule = Object.assign(CodingRuleAPI.codingRule, value);
  },
  init: () => new CodingRule(),
  getCodingRule(params) {
    return new Promise((resolve) => {
      IBASIC.codingRule.getCodingRule(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listCodingRule(params) {
    return new Promise((resolve) => {
      IBASIC.codingRule.listCodingRule(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertCodingRule(params) {
    return new Promise((resolve) => {
      IBASIC.codingRule.insertCodingRule(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateCodingRule(params) {
    return new Promise((resolve) => {
      IBASIC.codingRule.updateCodingRule(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteCodingRule(params) {
    return new Promise((resolve) => {
      IBASIC.codingRule.deleteCodingRule(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { CodingRuleAPI };
