import {FormConfigAPI} from "./base";
import {IBASIC} from "../api";

Object.assign(FormConfigAPI, {
  listFormKeyByFormConfig(params) {
    return new Promise((resolve) => {
      IBASIC.formConfig.listFormKeyByFormConfig(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {
          list = data.data.data;
        }
        resolve({data, list, res});
      });
    });
  }
});
export {
  FormConfigAPI
};
