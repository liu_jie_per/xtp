
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.UserMenuOperation;
import cn.xtits.xtp.entity.UserMenuOperationExample;
import cn.xtits.xtp.mapper.base.UserMenuOperationMapper;
import cn.xtits.xtp.service.UserMenuOperationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class UserMenuOperationServiceImpl implements UserMenuOperationService {

    @Resource
    private UserMenuOperationMapper userMenuOperationMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return userMenuOperationMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(UserMenuOperation record) {
        return userMenuOperationMapper.insert(record);
    }

    @Override
    public List<UserMenuOperation> listByExample(UserMenuOperationExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) userMenuOperationMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public UserMenuOperation getByPrimaryKey(Integer ID) {
        return userMenuOperationMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(UserMenuOperation record) {
        return userMenuOperationMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateUserMenuOperation(Integer userId, Integer menuId, List<UserMenuOperation> userMenuOperations) {
        UserMenuOperationExample example = new UserMenuOperationExample();
        UserMenuOperationExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andMenuIdEqualTo(menuId);
        List<UserMenuOperation> list = userMenuOperationMapper.selectByExample(example);
        for (UserMenuOperation userMenuOperation : list) {
            userMenuOperationMapper.deleteByPrimaryKey(userMenuOperation.getId());
        }
        for (UserMenuOperation userMenu : userMenuOperations) {
            userMenu.setUserId(userId);
            userMenu.setMenuId(menuId);
            userMenuOperationMapper.insert(userMenu);
        }
        return 1;
    }
}