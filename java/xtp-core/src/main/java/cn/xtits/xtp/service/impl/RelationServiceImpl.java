package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Relation;
import cn.xtits.xtp.entity.RelationExample;
import cn.xtits.xtp.mapper.base.RelationMapper;
import cn.xtits.xtp.service.RelationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Generator on 2018-07-16 06:52:24
 */
@Service
public class RelationServiceImpl implements RelationService {

    @Resource
    private RelationMapper relationMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return relationMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Relation record) {
        return relationMapper.insertSelective(record);
    }

    @Override
    public List<Relation> listByExample(RelationExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) relationMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Relation getByPrimaryKey(Integer id) {
        return relationMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Relation record) {
        return relationMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(Relation record) {
        return relationMapper.updateByPrimaryKeySelective(record);
    }


}