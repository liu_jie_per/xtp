
package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.UserMenu;
import cn.xtits.xtp.entity.UserMenuExample;
import cn.xtits.xtp.mapper.base.UserMenuMapper;
import cn.xtits.xtp.service.UserMenuService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ShengHaiJiang on 2017/3/7.
 */
@Service
public class UserMenuServiceImpl implements UserMenuService {

    @Resource
    private UserMenuMapper userMenuMapper;


    @Override
    public int deleteByPrimaryKey(Integer ID) {
        return userMenuMapper.deleteByPrimaryKey(ID);
    }

    @Override
    public int insert(UserMenu record) {
        return userMenuMapper.insert(record);
    }

    @Override
    public List<UserMenu> listByExample(UserMenuExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) userMenuMapper.selectByExample(example);
        example.setCount((int) page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public UserMenu getByPrimaryKey(Integer ID) {
        return userMenuMapper.selectByPrimaryKey(ID);
    }

    @Override
    public int updateByPrimaryKey(UserMenu record) {
        return userMenuMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateUserMenu(Integer userId, List<UserMenu> userMenus) {
        UserMenuExample example = new UserMenuExample();
        UserMenuExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        List<UserMenu> list = userMenuMapper.selectByExample(example);
        for (UserMenu userMenu : list) {
            userMenuMapper.deleteByPrimaryKey(userMenu.getId());
        }
        for (UserMenu userMenu : userMenus) {
            userMenu.setUserId(userId);
            userMenuMapper.insert(userMenu);
        }
        return 1;
    }
}