package cn.xtits.xtp.mapper;

import cn.xtits.xtp.entity.App;
import org.apache.ibatis.annotations.Param;

public interface AppExtMapper {
    App getAppByToken(@Param("token") String token);

    App getAppByCode(@Param("code") String code);
}
