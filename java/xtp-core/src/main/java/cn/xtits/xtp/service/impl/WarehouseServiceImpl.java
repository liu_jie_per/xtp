package cn.xtits.xtp.service.impl;

import cn.xtits.xtp.entity.Warehouse;
import cn.xtits.xtp.entity.WarehouseExample;
import cn.xtits.xtp.mapper.base.WarehouseMapper;
import cn.xtits.xtp.service.WarehouseService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dan on 2017-12-08 10:37:10
 */
@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Resource
    private WarehouseMapper mapper;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Warehouse record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<Warehouse> listByExample(WarehouseExample example) {
        PageHelper.startPage(example.getPageIndex().intValue(), example.getPageSize().intValue());
        Page page = (Page) mapper.selectByExample(example);
        example.setCount((int)page.getTotal());
        return page.toPageInfo().getList();
    }

    @Override
    public Warehouse getByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(Warehouse record) {
        return mapper.updateByPrimaryKey(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(Warehouse record) {
        return mapper.updateByPrimaryKeySelective(record);
    }
}