package cn.xtits.xtp.service;

import cn.xtits.xtp.entity.CodingRule;
import cn.xtits.xtp.entity.CodingRuleExample;

import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 2017/8/21.
 */
public interface CodingRuleService {

    Map<String, Object> getAssemblyCoding(String key);

    int deleteByPrimaryKey(Integer id);

    int insert(CodingRule record);

    List<CodingRule> listByExample(CodingRuleExample example);

    CodingRule getByPrimaryKey(Integer id);

    int updateByPrimaryKey(CodingRule record);

    int updateByPrimaryKeySelective(CodingRule record);
}
