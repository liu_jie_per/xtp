package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.CodingRule;
import cn.xtits.xtp.entity.CodingRuleExample;
import cn.xtits.xtp.service.CodingRuleService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @fileName: CodingRuleController.java
 * @author: Dan
 * @createDate: 2018-03-29 20:52:18
 * @description: 编码规则
 */
@RestController
@RequestMapping("/codingRule")
public class CodingRuleController extends BaseController {

    Gson gson = new GsonBuilder().serializeNulls().create();

    @Autowired
    private CodingRuleService service;

    @RequestMapping(value = "getAssemblyCoding")
    public AjaxResult getAssemblyCoding(
            @RequestParam(value = "key", required = true) String key) {
        Map<String, Object> assemblyCodingMap = service.getAssemblyCoding(key);
        return new AjaxResult(assemblyCodingMap);
    }

    @RequiresPermissions("ibasic-coding-rule:insert")
    @RequestMapping(value = "insertCodingRule", method = RequestMethod.POST)
    public AjaxResult insertCodingRule(
            @RequestParam(value = "data", required = false) String data) {
        CodingRule record = gson.fromJson(data, CodingRule.class);
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        service.insert(record);
        return new AjaxResult(1);
    }

    @RequiresPermissions("ibasic-coding-rule:delete")
    @RequestMapping(value = "deleteCodingRule")
    public AjaxResult deleteCodingRule(
            @RequestParam(value = "id", required = false) int id) {
        CodingRule record = new CodingRule();
        record.setId(id);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(true);
        service.updateByPrimaryKeySelective(record);
        return new AjaxResult(1);
    }

    @RequiresPermissions("ibasic-coding-rule:update")
    @RequestMapping(value = "updateCodingRule", method = RequestMethod.POST)
    public AjaxResult updateCodingRule(
            @RequestParam(value = "data", required = false) String data) {
        CodingRule record = gson.fromJson(data, CodingRule.class);
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        service.updateByPrimaryKeySelective(record);
        return new AjaxResult(1);
    }


    @RequestMapping(value = "listCodingRule")
    public AjaxResult listCodingRule(
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "ruleCode", required = false) String ruleCode,
            @RequestParam(value = "ruleName", required = false) String ruleName,
            @RequestParam(value = "type", required = false) Integer type,
            @RequestParam(value = "length", required = false) Integer length,
            @RequestParam(value = "initValue", required = false) String initValue,
            @RequestParam(value = "format", required = false) String format,
            @RequestParam(value = "sort", required = false) Integer sort,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter formatDime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        CodingRuleExample example = new CodingRuleExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        CodingRuleExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteFlagEqualTo(false);

        if (StringUtils.isNotBlank(key)) {
            criteria.andKeyLike(key);
        }
        if (StringUtils.isNotBlank(ruleCode)) {
            criteria.andRuleCodeLike(ruleCode);
        }
        if (StringUtils.isNotBlank(ruleName)) {
            criteria.andRuleNameLike(ruleName);
        }
        if (type != null) {
            criteria.andTypeEqualTo(type);
        }
        if (length != null) {
            criteria.andLengthEqualTo(length);
        }
        if (StringUtils.isNotBlank(initValue)) {
            criteria.andInitValueLike(initValue);
        }
        if (StringUtils.isNotBlank(format)) {
            criteria.andFormatLike(format);
        }
        if (sort != null) {
            criteria.andSortEqualTo(sort);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, formatDime).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, formatDime).toDate());
        }

        List<CodingRule> list = service.listByExample(example);
        Pagination<CodingRule> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getCodingRule")
    public AjaxResult getCodingRule(
            @RequestParam(value = "id", required = false) Integer id) {
        CodingRule res = service.getByPrimaryKey(id);
        return new AjaxResult(res);
    }
}