package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.Dict;
import cn.xtits.xtp.entity.DictExample;
import cn.xtits.xtp.enums.ErrorCodeEnums;
import cn.xtits.xtp.service.DictService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @fileName: DictController.java
 * @author: Dan
 * @createDate: 2018-02-28 13:43:14
 * @description: 字典
 */
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {

    @Autowired
    private DictService service;

    @RequiresPermissions("ibasic-dict:insert")
    @RequestMapping(value = "insertDict")
    public AjaxResult insertDict(
            @RequestParam(value = "data", required = false) String data) {
        Dict record = JsonUtil.fromJson(data, Dict.class);
        if (exist(record) > 0) {
            return new AjaxResult(ErrorCodeEnums.RECORD_EXISTS.value, ErrorCodeEnums.RECORD_EXISTS.msg);
        }
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);

        if (record.getEditFlag() == null) {
            record.setEditFlag(true);
        }
        if (record.getDisplayFlag() == null) {
            record.setDisplayFlag(true);
        }
        if (record.getSort() == null) {
            record.setSort(1);
        }
        int row = service.insert(record);
        return new AjaxResult(row);
    }

    @RequiresPermissions("ibasic-dict:delete")
    @RequestMapping(value = "deleteDict")
    public AjaxResult deleteDict(
            @RequestParam(value = "id", required = false) int id) {
        Dict record = new Dict();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = service.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequiresPermissions("ibasic-dict:update")
    @RequestMapping(value = "updateDict")
    public AjaxResult updateDict(
            @RequestParam(value = "data", required = false) String data) {
        Dict record = JsonUtil.fromJson(data, Dict.class);
        if (exist(record) > 0) {
            return new AjaxResult(ErrorCodeEnums.RECORD_EXISTS.value, ErrorCodeEnums.RECORD_EXISTS.msg);
        }
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);

        record.setEditFlag(null);
        record.setDisplayFlag(null);
        int row = service.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequestMapping(value = "listDict")
    public AjaxResult listDict(
            @RequestParam(value = "editFlag", required = false) Boolean editFlag,
            @RequestParam(value = "displayFlag", required = false) Boolean displayFlag,
            @RequestParam(value = "sort", required = false) Integer sort,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "keyName", required = false) String keyName,
            @RequestParam(value = "value", required = false) String value,
            @RequestParam(value = "remark", required = false) String remark,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DictExample example = new DictExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        DictExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (editFlag != null) {
            criteria.andEditFlagEqualTo(editFlag);
        }
        if (displayFlag != null) {
            criteria.andDisplayFlagEqualTo(displayFlag);
        }
        if (sort != null) {
            criteria.andSortEqualTo(sort);
        }
        if (StringUtils.isNotBlank(keyName)) {
            criteria.andKeyNameLike(keyName);
        }
        if (StringUtils.isNotBlank(type)) {
            criteria.andTypeLike(type);
        }
        if (StringUtils.isNotBlank(key)) {
            criteria.andKeyLike(key);
        }
        if (StringUtils.isNotBlank(value)) {
            criteria.andValueLike(value);
        }
        if (StringUtils.isNotBlank(remark)) {
            criteria.andRemarkLike(remark);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<Dict> list = service.listByExample(example);
        Pagination<Dict> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getDict")
    public AjaxResult getDict(
            @RequestParam(value = "id", required = false) Integer id) {
        Dict res = service.getByPrimaryKey(id);
        return new AjaxResult(res);
    }

    /**
     * 是否存在相同数据
     *
     * @param entity
     * @return 返回相同数据的条数
     */
    private int exist(Dict entity) {
        DictExample example = new DictExample();
        example.setPageSize(1);
        DictExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteFlagEqualTo(false);
        if (null != entity.getId() && entity.getId() > 0) {
            criteria.andIdNotEqualTo(entity.getId());
        }
        if (entity.getType() != null) {
            criteria.andTypeEqualTo(entity.getType());
        }
        if (entity.getKey() != null) {
            criteria.andKeyEqualTo(entity.getKey());
        }
        return service.listByExample(example).size();
    }

}

